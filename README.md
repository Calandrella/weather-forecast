# Weather Forecast App
RESTfull API service which retrieve some average weather info based on the data exposed by [Open Weather](https://openweathermap.org/).
It provides:
1. Average of daily (6h-18h) temperature, in Celsius, for the following 3 days
2. Average of nightly (18h-6h) temperature, in Celsius, for the following 3 days
3. Average of pressure for the following 3 days

Since it's RESTfull all the client needs to know is the entry point of the app the "/data" endpoint.

# Introduction
RESTfull architecture is base on Roy Fielding [PHD dissertation (chapter 5)](https://www.ics.uci.edu/~fielding/pubs/dissertation/fielding_dissertation.pdf).
The main focus is on resources and hypermedia driven approach.
"The Representational State Transfer (REST) style is an abstraction of the architectural
 elements within a distributed hypermedia system. REST ignores the details of component
 implementation and protocol syntax in order to focus on the roles of components, the
 constraints upon their interaction with other components, and their interpretation of
 significant data elements. It encompasses the fundamental constraints upon components,
 connectors, and data that define the basis of the Web architecture, and thus the essence of
 its behavior as a network-based application."

# Getting started
To better fit a RESTfull approach the entryPoint of the api ("/data") provide info about all available cities .
From here the client can navigate the whole api using the links provided: a must in RESTfull services is
[HATEOAS](https://en.wikipedia.org/wiki/HATEOAS). I didn't follow a standard (like HAL) and decided to go
for self link into HEADER and other links into the body (I also added a "METHOD" param that I know
be useful for clients to avoid additional an OPTION call).

I decided to adopt an hexagonal architecture approach for this microservice in order to have
better module isolation. This way I can easily change the services used (like the one that retrieve all 
the available cities) without any impact on the core app code.

I used the monad pattern to implement validation. I also used the converter pattern to manage object 
to resource conversion (even if it's a microservice so maybe it's overkilling...)
In order to cache some data I used a well know library [Caffeine](https://github.com/ben-manes/caffeine).
The caching implementation highlights the Single Responsibility Principle. Anyway there is still the need
for a CacheManager to avoid creating cache into our services.

I tried to avoid using frameworks as much as possible. The only real framework used is Jersey (to be 
easily compliant with JAX-RS spec).
Every component is tested in Isolation. I used DI by composing the app when actually instantiating 
the server and publishing resources on it.

I added both formatting and code coverage metrics (must be part of a CI pipeline), u can see coverage by running the "jacocoReport"
task in gradle (after test) and opening the report in your favourite browser (it's under build/report/jacoco)
Some of the actual tests (those involving jetty) are actually integration test (even if mocking some service).
I decided to run it with the actual unit test since they are very fast and provide feedback to the dev.

It's needed to improve the client for Open weather api, since it's not working with clear separation of concerns
(both client and converter are in the same method). Anyway it will be an easy refactoring and it will
increase the coverage of the conversion part that now is partially handled by integration tests.

The application is providing a naive pagination for making RESTfully available all resources exposed by
Open Weather apis. As said before regarding HATEOAS, also here next page links are provided inside header.

###### It is strongly recommended to use the pagination when accessing the root resource (e.g. "/data?offset=0&limit=1000")

##Run the application
U can easily run the application by using the container shipped with it. It's of course a naive implementation
but, as visible, u can run every gradle command (by using the wrapper).
This means the image is suitable also for being used inside a CI pipeline (linting, test, build, integration).
To run it locally u can use the following command:
```bash
docker build -t weatherrunner . && docker run -d --rm --mount "type=bind,src=##absolutePathToProjectSourceFolder##,dst=/opt/app"  -p8090:8080/tcp weatherrunner "./gradlew clean  &
& ./gradlew assemble && java -jar app/build/libs/app-1.0.jar"
```

and it will be available on your host at 8090 port. Of course it is possible to add a simple ngnix proxy for a better
deployment.

NOTE: if u already made the first build with gradle, u maybe will need to first clean otherwise u'll have 
users/permission issues (especially on Windows).

