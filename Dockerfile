FROM openjdk:8-jdk-alpine

RUN apk add --no-cache bash
RUN apk --no-cache add shadow


ENV appPort 8080
ENV appKey 0bdeb1c4dd0f7b4dee45cac142dbc80e
EXPOSE 8080

RUN groupadd -r raf -g 1100 && useradd -u 1100 -r -g raf -m -s /sbin/nologin -c "Test user" raf
RUN mkdir --mode=755 /opt
RUN mkdir /opt/app && chmod a+rwx /opt/app

USER raf
WORKDIR /opt/app

ENTRYPOINT ["/bin/bash", "-c"]
