package it.coderkahn.patterns.monad;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.stream.Stream;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class ValidatorTest {

  private static Stream<BigDecimal> salaries() {
    return Stream.of(new BigDecimal(1), new BigDecimal(14121), new BigDecimal(998));
  }

  @Test
  void canCreateValidator() {
    Assertions.assertNotNull(Validator.of(new Object()));
  }

  @Test
  void canApplySingleValidationFunction() {
    Throwable t =
        Assertions.assertThrows(
            RuntimeException.class,
            () -> Validator.of(null).validate(Objects::nonNull, "Null Object").get());
    MatcherAssert.assertThat(t.getMessage(), Matchers.containsString("Null Object"));
  }

  @Test
  void canApplyMultipleValidationAndGetTheResultsOfAllOfThem() {
    String message =
        Assertions.assertThrows(
                RuntimeException.class,
                () ->
                    Validator.of("Raffaele")
                        .validate(Objects::nonNull, "Null Object")
                        .validate(
                            PredicateUtil.negate(s -> s.contains("R")), "String does contains R")
                        .validate(PredicateUtil.negate(s -> s.length() < 10), "Invalid length")
                        .get())
            .getMessage();
    MatcherAssert.assertThat(message, Matchers.containsString("String does contains R"));
    MatcherAssert.assertThat(message, Matchers.containsString("Invalid length"));
  }

  @ParameterizedTest
  @MethodSource("salaries")
  void canConcatFunctionsOnValidatedObjects(BigDecimal money) {
    BigDecimal amount = new BigDecimal(0);
    BigDecimal finalAmount =
        amount.add(
            Validator.of(money)
                .validate(i -> i.compareTo(BigDecimal.ZERO) > 0, "Amount must be positive")
                .get());
    MatcherAssert.assertThat(finalAmount, Matchers.greaterThan(amount));
  }

  static class Wallet {
    private BigDecimal amount = new BigDecimal(0);

    public void add(BigDecimal money) {
      amount =
          amount.add(
              Validator.of(money)
                  .validate(i -> i.compareTo(BigDecimal.ZERO) > 0, "Amount must be positive")
                  .get());
    }

    public BigDecimal currentValue() {
      return this.amount;
    }
  }
}
