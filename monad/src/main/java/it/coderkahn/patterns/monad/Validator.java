package it.coderkahn.patterns.monad;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Validator<T> {
  private final T instance;

  private List<String> messages = new ArrayList<>();

  private Validator(T instance) {
    this.instance = instance;
  }

  public static <T> Validator<T> of(T instance) {
    return new Validator<>(instance);
  }

  public final Validator<T> validate(Predicate<T> validationPredicate, String message) {
    if (!validationPredicate.test(instance)) {
      messages.add(message);
    }
    return this;
  }

  public T get() {
    if (!messages.isEmpty()) {
      throw new InvalidInstanceException(
          instance,
          messages
              .stream()
              .reduce((s, s2) -> String.format("%s. %s", s, s2))
              .orElse(String.format("Invalid object %s", instance)));
    }
    return instance;
  }

  private static class InvalidInstanceException extends RuntimeException {
    private final Object instance;
    private final String message;

    private InvalidInstanceException(Object instance, String message) {
      this.instance = instance;
      this.message = message;
    }

    public String getMessage() {
      return String.format("Object instance %s is not valid. ", instance).concat(message);
    }
  }
}
