package it.coderkahn.patterns.monad;

import java.util.function.Predicate;

public class PredicateUtil {
  /**
   * Java 11 provide this helper to apply negate...
   *
   * @param predicate
   * @param <T>
   * @return
   */
  public static <T> Predicate<T> negate(Predicate<T> predicate) {
    return predicate.negate();
  }
}
