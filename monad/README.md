## Purpose 
A [Monad](https://en.wikipedia.org/wiki/Monad_(category_theory)) is an endofunctor with 2 natural transformation: 
one is the identity function (return) and the other is the bind operator
that allows for the concatenation of monads of the same type (composition). 
Monad [born](https://www.sciencedirect.com/science/article/pii/0890540191900524) 
as a way to perform side effects (I/O) in pure functional programming languages and it's a useful abstraction
to chain operations. 

# Use cases
Whenever u want to chain operations on some Object and apply all of them regardless the results of the previous one.

An easy use case can be object validation (think about forms), where u can be in the need of validating
more than one field and collect the results before telling the client what went wrong in his call.