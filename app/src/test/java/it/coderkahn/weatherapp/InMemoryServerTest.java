package it.coderkahn.weatherapp;

import java.io.IOException;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class InMemoryServerTest {

  private InMemoryServer server;

  private int port;

  @BeforeEach
  void setUp() throws IOException {
    port = ServerlessUtils.findFreePort();
    server = InMemoryServer.create(InMemoryServerTest.TestApp.class);
  }

  @AfterEach
  void tearDown() throws Exception {
    server.close();
  }

  @Test
  void exceptionIfUsingNullApplication() {
    Assertions.assertThrows(RuntimeException.class, () -> InMemoryServer.create(null));
  }

  @Test
  void AppPathRetrieveFromJaxRsAnnotation() {
    MatcherAssert.assertThat(
        InMemoryServer.create(InMemoryServerTest.TestApp.class).getAppPath(),
        Matchers.endsWith("/test"));
  }

  @Test
  void AppPathDefaultsIfNoneIsSpecifiedInJaxRsAnnotation() {
    InMemoryServer server = InMemoryServer.create(Application.class);
    MatcherAssert.assertThat(server.getAppPath(), Matchers.equalTo("/"));
  }

  @ApplicationPath("/test")
  public static class TestApp extends Application {}
}
