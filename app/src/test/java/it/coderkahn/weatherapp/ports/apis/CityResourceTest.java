package it.coderkahn.weatherapp.ports.apis;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CityResourceTest {

  @Test
  void canCreateCity() {
    CityResource cityResource = new CityResource(123, "Milan", "IT", 36.3, 23.5);
    Assertions.assertEquals(123, cityResource.getId());
    Assertions.assertEquals("Milan", cityResource.getName());
    Assertions.assertEquals("IT", cityResource.getCountry());
    Assertions.assertEquals(36.3, cityResource.getLongitude());
    Assertions.assertEquals(23.5, cityResource.getLatitude());
  }

  @Test
  void cityMustHaveNonZeroId() {
    Assertions.assertThrows(
        RuntimeException.class, () -> new CityResource(0, "some", "some", 23, 23));
  }

  @Test
  void cityNameCanBeEmptyButNotNull() {
    Throwable t =
        Assertions.assertThrows(
            RuntimeException.class, () -> new CityResource(1, null, null, 23, 23));
    MatcherAssert.assertThat(t.getMessage(), Matchers.containsString("name"));
    MatcherAssert.assertThat(t.getMessage(), Matchers.containsString("cannot be null"));
    Assertions.assertDoesNotThrow(
        () -> {
          new CityResource(1, "", "", 23, 23);
        });
  }
}
