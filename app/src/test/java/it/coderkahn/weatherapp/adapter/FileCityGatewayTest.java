package it.coderkahn.weatherapp.adapter;

import it.coderkahn.weatherapp.core.City;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FileCityGatewayTest {

  @Test
  void whenFileNotFoundExceptionThrown() {
    Assertions.assertThrows(RuntimeException.class, () -> new FileCityGateway("testFile"));
  }

  @Test
  void canCreateFileCityGw() {
    Assertions.assertDoesNotThrow(() -> new FileCityGateway("test.file.json"));
  }

  @Test
  void canParseCityData() {
    FileCityGateway fileCityGateway = new FileCityGateway("test.file.json");
    List<City> cities = fileCityGateway.getAvailableCities();
    Assertions.assertNotNull(cities);
    Assertions.assertEquals(209579, cities.size());
  }
}
