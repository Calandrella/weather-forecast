package it.coderkahn.weatherapp.adapter;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.withArgs;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import it.coderkahn.weatherapp.InMemoryServer;
import it.coderkahn.weatherapp.ServerlessUtils;
import java.io.IOException;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RestCityWeatherAdapterTest {

  public static final String ENTRY_POINT = "data";

  private InMemoryServer server;

  private int port;

  private String baseUri;

  public static final int MILAN_ID = 3173435;

  @BeforeEach
  void setUp() throws IOException {
    port = ServerlessUtils.findFreePort();
    server = InMemoryServer.create(TestApp.class);
    server.start(port);
    baseUri = "http://127.0.0.1:" + port + server.getAppPath();
  }

  @AfterEach
  void tearDown() throws Exception {
    server.close();
  }

  @Test
  void defaultExceptionReturnsInternalServerError() {
    givenTheBaseUri()
        .log()
        .all(true)
        .get("exception")
        .then()
        .statusCode(500)
        .header("Content-Type", "application/json")
        .body(Matchers.containsString("EXCEPTION!"));
  }

  @Test
  void selfLinkInTheHeaderForEntryPoint() {
    givenTheBaseUri()
        .log()
        .all(true)
        .get(ENTRY_POINT)
        .then()
        .header("Link", Matchers.containsString(restEntryPoint()))
        .header("Link", Matchers.containsString("rel=\"self\""))
        .header("Link", Matchers.containsString("rel=\"self\""));
  }

  @Test
  void resourceValuesHandledCorrectly() {
    givenTheBaseUri()
        .log()
        .all(true)
        .get(ENTRY_POINT)
        .then()
        .root("find {it.id == %d}")
        .body("name", withArgs(MILAN_ID), is("Milano"))
        .body("country", withArgs(MILAN_ID), is("IT"))
        .body("latitude", withArgs(MILAN_ID), equalTo(45.464272f))
        .body("longitude", withArgs(MILAN_ID), equalTo(9.18951f));
  }

  @Test
  void limitAndOffsetWorks() {
    givenTheBaseUri()
        .log()
        .all(true)
        .queryParam("limit", "1")
        .get(ENTRY_POINT)
        .then()
        .body("id", Matchers.hasSize(1))
        .body("name", Matchers.contains("Milano"));
  }

  @Test
  void canAccessDataById() {
    int milanId = 3173435;
    givenTheBaseUri()
        .log()
        .all(true)
        .get(ENTRY_POINT + "/" + milanId)
        .then()
        .body("dailyAverage", Matchers.equalTo(27.3f))
        .body("nightlyAverage", Matchers.equalTo(18.4f))
        .body("pressure", Matchers.equalTo(2f));
  }

  @Test
  void selfLinkInTheHeaderForSubResource() {
    int milanId = 3173435;
    givenTheBaseUri()
        .log()
        .all(true)
        .get(ENTRY_POINT + "/" + milanId)
        .then()
        .header("Link", Matchers.containsString(restEntryPoint() + "/" + milanId))
        .header("Link", Matchers.containsString("rel=\"self\""));
  }

  @Test
  void invalidIdReturnsNotFoundError() {
    givenTheBaseUri().log().all(true).get(ENTRY_POINT + "/" + 23719328).then().statusCode(404);
  }

  private String restEntryPoint() {
    return baseUri + ENTRY_POINT;
  }

  private RequestSpecification givenTheBaseUri() {
    return given()
        .filter(new RequestLoggingFilter())
        .filter(new ErrorLoggingFilter())
        .filter(new ResponseLoggingFilter())
        .baseUri(baseUri);
  }
}
