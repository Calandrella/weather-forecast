package it.coderkahn.weatherapp.adapter.converters;

import it.coderkahn.weatherapp.adapter.CachedCityGw;
import it.coderkahn.weatherapp.core.City;
import it.coderkahn.weatherapp.ports.CityGateway;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class CachedCityGwTest {
  private CityGateway cityGatewayMock;
  private CachedCityGw cachedCityGw;

  @BeforeEach
  void setUp() {
    cityGatewayMock = Mockito.mock(CityGateway.class);
    cachedCityGw = new CachedCityGw(cityGatewayMock);
  }

  @Test
  void firstCallDelegate() {
    Mockito.when(cityGatewayMock.getAvailableCities()).thenReturn(Collections.emptyList());
    cachedCityGw.getAvailableCities();
    Mockito.verify(cityGatewayMock, Mockito.times(1)).getAvailableCities();
  }

  @Test
  void secondCallRetrieveFromCache() {
    City city = new City();
    Mockito.when(cityGatewayMock.getAvailableCities())
        .thenReturn(Stream.of(city).collect(Collectors.toList()));
    cachedCityGw.getAvailableCities();
    List<City> cities = cachedCityGw.getAvailableCities();
    Mockito.verify(cityGatewayMock, Mockito.times(1)).getAvailableCities();
    Assertions.assertNotNull(cities);
    Assertions.assertEquals(1, cities.size());
    Assertions.assertEquals(city, cities.get(0));
  }
}
