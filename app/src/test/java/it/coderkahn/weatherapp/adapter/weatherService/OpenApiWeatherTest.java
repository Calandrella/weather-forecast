package it.coderkahn.weatherapp.adapter.weatherService;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static it.coderkahn.weatherapp.CityResourceProvider.MILAN;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import it.coderkahn.weatherapp.core.converters.CityConverter;
import it.coderkahn.weatherapp.ports.apis.WeatherMetrics;
import java.io.IOException;
import java.net.ServerSocket;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import joptsimple.internal.Strings;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class OpenApiWeatherTest {

  private int port;

  private WireMockServer server;

  @BeforeEach
  void setUp() throws IOException {
    this.port = findFreePort();
    server = new WireMockServer(port);
    WireMock.configureFor(port);
    server.start();
    server.stubFor(
        get(urlPathEqualTo("/data/forecast"))
            .withQueryParam("id", equalTo(Integer.toString(MILAN.getId())))
            .withQueryParam("units", equalTo(Query.Unit.METRIC.getStringValue()))
            .willReturn(
                aResponse()
                    .withBody(OpenApiResponseSupplier.getResponse())
                    .withHeader("Content-Type", "application/json")));
  }

  private static int findFreePort() throws IOException {
    ServerSocket server = new ServerSocket(0);
    int port = server.getLocalPort();
    server.close();
    return port;
  }

  @AfterEach
  void tearDown() {
    if (server.isRunning()) server.stop();
  }

  @Test
  void canInvokeApi() {
    Assertions.assertTimeout(
        Duration.ofSeconds(1),
        () -> {
          OpenApiWeather api = new OpenApiWeather("http://127.0.0.1:" + port + "/data");
          WeatherMetrics weatherMetrics =
              api.getWeatherMetrics(new CityConverter().convertFromResource(MILAN), 3);
          Assertions.assertNotNull(weatherMetrics);
          WireMock.verify(
              1,
              getRequestedFor(urlPathEqualTo("/data/forecast"))
                  .withQueryParam("id", equalTo(Integer.toString(MILAN.getId())))
                  .withQueryParam("units", equalTo(Query.Unit.METRIC.getStringValue())));
        });
  }

  @Test
  void canComputeAverage() {
    Assertions.assertTimeout(
        Duration.ofSeconds(1),
        () -> {
          OpenApiWeather api = new OpenApiWeather("http://127.0.0.1:" + port + "/data");
          WeatherMetrics weatherMetrics =
              api.getWeatherMetrics(new CityConverter().convertFromResource(MILAN), 3);
          Assertions.assertEquals(5.14, weatherMetrics.getNightlyAverage());
          Assertions.assertEquals(5.14, weatherMetrics.getDailyAverage());
          Assertions.assertEquals(MILAN.getName(), weatherMetrics.getCityName());
          Assertions.assertEquals(MILAN.getCountry(), weatherMetrics.getCountry());
          MatcherAssert.assertThat(
              weatherMetrics.getPressure(), Matchers.lessThanOrEqualTo(10003.26));
        });
  }

  private static class OpenApiResponseSupplier {

    public static final String DATA =
        "{\n"
            + "      \"dt\": %d,\n"
            + "      \"main\": {\n"
            + "        \"temp\": 5.14,\n"
            + "        \"temp_min\": 3.78,\n"
            + "        \"temp_max\": 5.14,\n"
            + "        \"pressure\": 1003.26,\n"
            + "        \"sea_level\": 1030.73,\n"
            + "        \"grnd_level\": 1003.26,\n"
            + "        \"humidity\": 100,\n"
            + "        \"temp_kf\": 1.36\n"
            + "      },\n"
            + "      \"weather\": [\n"
            + "        {\n"
            + "          \"id\": 500,\n"
            + "          \"main\": \"Rain\",\n"
            + "          \"description\": \"light rain\",\n"
            + "          \"icon\": \"10n\"\n"
            + "        }\n"
            + "      ],\n"
            + "      \"clouds\": {\n"
            + "        \"all\": 44\n"
            + "      },\n"
            + "      \"wind\": {\n"
            + "        \"speed\": 1.37,\n"
            + "        \"deg\": 8.50171\n"
            + "      },\n"
            + "      \"rain\": {\n"
            + "        \"3h\": 0.11\n"
            + "      },\n"
            + "      \"sys\": {\n"
            + "        \"pod\": \"n\"\n"
            + "      },\n"
            + "      \"dt_txt\": \"%s\"\n"
            + "    }";

    public static String getResponse() {
      DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
      String init =
          "{\n"
              + "  \"cod\": \"200\",\n"
              + "  \"message\": 0.0024,\n"
              + "  \"cnt\": 40,\n"
              + "  \"list\": [";
      String end = "\n" + "  ]\n" + "}";
      LocalDateTime now = LocalDateTime.now();
      LocalDateTime initialTime =
          LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), now.getHour(), 0, 0);

      return init
          + Strings.join(
              IntStream.range(0, 30)
                  .mapToObj(
                      value ->
                          String.format(
                              DATA,
                              initialTime
                                  .plus(value * 3, ChronoUnit.HOURS)
                                  .toEpochSecond(ZoneOffset.UTC),
                              format.format(initialTime.plus(value * 3, ChronoUnit.HOURS))))
                  .collect(Collectors.toList()),
              ",")
          + end;
    }
  }
}
