package it.coderkahn.weatherapp.adapter.weatherService;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FcByCityIdTest {

  @Test
  void endpointExpectedByCityId() {
    FcByCityId fcQuery = new FcByCityId("http://api.openweathermap.org/data/2.5", 123);
    Assertions.assertEquals(
        "http://api.openweathermap.org/data/2.5/forecast?id=123&units=metric&appid=API_KEY",
        fcQuery.endpointUri("API_KEY"));
  }
}
