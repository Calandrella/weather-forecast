package it.coderkahn.weatherapp.adapter;

import static it.coderkahn.weatherapp.CityResourceProvider.MILAN;
import static it.coderkahn.weatherapp.CityResourceProvider.ROME;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import it.coderkahn.weatherapp.ports.apis.CityResource;
import it.coderkahn.weatherapp.ports.apis.WeatherMetrics;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

@ApplicationPath("/")
public class TestApp extends Application {
  @Override
  public Set<Object> getSingletons() {
    List<CityResource> cities = Stream.of(MILAN, ROME).collect(Collectors.toList());
    return Collections.unmodifiableSet(
        Stream.of(
                new JacksonJaxbJsonProvider(),
                new ResourceNotFoundHandler(),
                new DefaultExceptionHandler(),
                new ExceptionService(),
                new RestCityWeatherAdapter(
                    () -> cities,
                    (city, forecastDaysNr) -> {
                      cities
                          .stream()
                          .filter(c -> c.getId() == city.getId())
                          .findFirst()
                          .orElseThrow(() -> new ResourceNotFoundException("Not found"));
                      return new WeatherMetrics("CityName", "Country", 27.3, 18.4, 2);
                    }))
            .collect(Collectors.toSet()));
  }

  @Path("/exception")
  public static class ExceptionService {

    @GET
    @Produces("application/json")
    public Response response() {
      throw new RuntimeException("EXCEPTION!!!");
    }
  }
}
