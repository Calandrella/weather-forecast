package it.coderkahn.weatherapp.core;

import it.coderkahn.weatherapp.ports.CityGateway;
import it.coderkahn.weatherapp.ports.apis.CityResource;
import it.coderkahn.weatherapp.ports.apis.CityService;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class CityServiceImplTest {

  public static final City TEST_CITY = new City(1, "name", "country", new Coord(12.2, 12.2));
  public static final City TEST_CITY2 = new City(2, "name", "country", new Coord(12.2, 12.2));

  private CityService cityService;
  private CityGateway cityGatewayMock;

  @BeforeEach
  void setUp() {
    cityGatewayMock = Mockito.mock(CityGateway.class);
    cityService = new CityServiceImpl(cityGatewayMock);
  }

  @Test
  void exceptionThrowWhenNullRetrieved() {
    Mockito.when(cityGatewayMock.getAvailableCities()).thenReturn(null);
    Throwable t = Assertions.assertThrows(RuntimeException.class, () -> cityService.getCities());
    MatcherAssert.assertThat(t.getMessage(), Matchers.containsString("Not found"));
  }

  @Test
  void exceptionThrownWhenEmptyListRetrieved() {
    Mockito.when(cityGatewayMock.getAvailableCities()).thenReturn(Collections.emptyList());
    Throwable t = Assertions.assertThrows(RuntimeException.class, () -> cityService.getCities());
    MatcherAssert.assertThat(t.getMessage(), Matchers.containsString("Not found"));
  }

  @Test
  void actualListRetrieved() {
    Mockito.when(cityGatewayMock.getAvailableCities())
        .thenReturn(Collections.singletonList(TEST_CITY));
    List<CityResource> cities = cityService.getCities();
    Assertions.assertNotNull(cities);
    Assertions.assertEquals(1, cities.size());
    CityResource city = cities.get(0);
    Assertions.assertEquals(TEST_CITY.getId(), city.getId());
    Assertions.assertEquals(TEST_CITY.getCountry(), city.getCountry());
    Assertions.assertEquals(TEST_CITY.getName(), city.getName());
    Assertions.assertEquals(TEST_CITY.getLatitude(), city.getLatitude());
    Assertions.assertEquals(TEST_CITY.getLongitude(), city.getLongitude());
  }

  @Test
  void listIsSortedByIdAscending() {
    Mockito.when(cityGatewayMock.getAvailableCities())
        .thenReturn(Stream.of(TEST_CITY, TEST_CITY2).collect(Collectors.toList()));
    List<CityResource> cities = cityService.getCities();
    Assertions.assertNotNull(cities);
    Assertions.assertEquals(2, cities.size());
    Assertions.assertEquals(cities.get(0).getId(), TEST_CITY.getId());
    Assertions.assertEquals(cities.get(1).getId(), TEST_CITY2.getId());
  }

  @Test
  void exceptionInGatewayAreRethrown() {
    Mockito.when(cityGatewayMock.getAvailableCities()).thenThrow(new RuntimeException());
    Assertions.assertThrows(RuntimeException.class, () -> cityService.getCities());
  }
}
