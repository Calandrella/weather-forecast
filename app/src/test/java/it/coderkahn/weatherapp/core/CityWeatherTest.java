package it.coderkahn.weatherapp.core;

import static it.coderkahn.weatherapp.CityResourceProvider.MILAN;

import it.coderkahn.weatherapp.ports.WeatherApi;
import it.coderkahn.weatherapp.ports.apis.CityResource;
import it.coderkahn.weatherapp.ports.apis.CityWeather;
import it.coderkahn.weatherapp.ports.apis.WeatherMetrics;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

class CityWeatherTest {

  private CityWeather cityWeather;

  private WeatherApi weatherApiMock;

  @BeforeEach
  void setUp() {
    weatherApiMock = Mockito.mock(WeatherApi.class);
    cityWeather = new CityWeatherImpl(weatherApiMock);
  }

  @Test
  @DisplayName("If invoked with null city value exception is thrown")
  void nullCityThrowsException() {
    Throwable t =
        Assertions.assertThrows(
            RuntimeException.class, () -> cityWeather.forecastWeatherMetrics(null, 3));
    MatcherAssert.assertThat(
        t.getMessage(), Matchers.containsString("CityResource cannot be null"));
  }

  @ParameterizedTest
  @ValueSource(ints = {-1, 4, 125, -32112, 0})
  void forecastDayMustBePositiveAndLessThenThree(int forecastDays) {
    Throwable t =
        Assertions.assertThrows(
            RuntimeException.class, () -> cityWeather.forecastWeatherMetrics(MILAN, forecastDays));
    MatcherAssert.assertThat(t.getMessage(), Matchers.containsString("from 1 to 3 days forecast"));
  }

  @Test
  void canGetForecastData() {
    Mockito.when(
            weatherApiMock.getWeatherMetrics(ArgumentMatchers.any(), ArgumentMatchers.anyInt()))
        .thenReturn(new WeatherMetrics("city", "country", 14, 14, 1000));
    WeatherMetrics actual =
        cityWeather.forecastWeatherMetrics(new CityResource(1, "city", "country", 14.5, 23.4), 3);
    Assertions.assertNotNull(actual);
    Assertions.assertEquals("city", actual.getCityName());
  }
}
