package it.coderkahn.weatherapp.core;

import static it.coderkahn.weatherapp.CityResourceProvider.MILAN;

import it.coderkahn.weatherapp.ports.apis.CityWeather;
import it.coderkahn.weatherapp.ports.apis.WeatherMetrics;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

class CachedCityWeatherTest {

  private CityWeather cityWeather;

  private CityWeather weatherServiceMock;

  @BeforeEach
  void setUp() {
    weatherServiceMock = Mockito.mock(CityWeather.class);
    cityWeather = new CachedCityWeather(weatherServiceMock, 10_000, 2, TimeUnit.SECONDS);
  }

  @Test
  void firstInvocationDelegate() {
    Mockito.when(
            weatherServiceMock.forecastWeatherMetrics(
                ArgumentMatchers.any(), ArgumentMatchers.anyInt()))
        .thenReturn(new WeatherMetrics("CityName", "Country", 23, 23, 2));
    cityWeather.forecastWeatherMetrics(MILAN, 3);
    Mockito.verify(weatherServiceMock, Mockito.times(1))
        .forecastWeatherMetrics(ArgumentMatchers.eq(MILAN), ArgumentMatchers.eq(3));
  }

  @Test
  void afterFirstCallRetrievesFromCache() {
    Mockito.when(
            weatherServiceMock.forecastWeatherMetrics(
                ArgumentMatchers.any(), ArgumentMatchers.anyInt()))
        .thenReturn(new WeatherMetrics("CityName", "Country", 23, 23, 2));
    WeatherMetrics weatherMetrics = cityWeather.forecastWeatherMetrics(MILAN, 3);
    Assertions.assertEquals(weatherMetrics, cityWeather.forecastWeatherMetrics(MILAN, 3));
    Mockito.verify(weatherServiceMock, Mockito.times(1))
        .forecastWeatherMetrics(ArgumentMatchers.eq(MILAN), ArgumentMatchers.eq(3));
  }

  @Test
  void whenExpiredInvokeAgainTheService() {
    cityWeather = new CachedCityWeather(weatherServiceMock, 10_000, 2, TimeUnit.MICROSECONDS);
    Mockito.when(
            weatherServiceMock.forecastWeatherMetrics(
                ArgumentMatchers.any(), ArgumentMatchers.anyInt()))
        .thenReturn(new WeatherMetrics("CityName", "Country", 23, 23, 2));
    WeatherMetrics weatherMetrics = cityWeather.forecastWeatherMetrics(MILAN, 3);
    Assertions.assertEquals(weatherMetrics, cityWeather.forecastWeatherMetrics(MILAN, 3));
    Mockito.verify(weatherServiceMock, Mockito.times(2))
        .forecastWeatherMetrics(ArgumentMatchers.eq(MILAN), ArgumentMatchers.eq(3));
  }
}
