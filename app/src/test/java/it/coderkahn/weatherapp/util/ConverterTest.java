package it.coderkahn.weatherapp.util;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

class ConverterTest {

  private Function<Model, Resource> fromModel;

  private Function<Resource, Model> fromResource;

  private Converter<Model, Resource> converter;

  private static class Model {}

  private static class Resource {}

  @BeforeEach
  void setUp() {
    fromModel = Mockito.mock(Function.class);
    fromResource = Mockito.mock(Function.class);
    converter = new Converter<>(fromModel, fromResource);
  }

  @Test
  void invokeFunctionWhenConvertingFromModelToResource() {
    Model m = new Model();
    Resource r = new Resource();
    Mockito.when(fromModel.apply(m)).thenReturn(r);
    Assertions.assertEquals(r, converter.convertFromModels(m));
    Mockito.verify(fromModel, Mockito.times(1)).apply(ArgumentMatchers.eq(m));
  }

  @Test
  void invokeFunctionWhenConvertingFromResourceToModel() {
    Model m = new Model();
    Resource r = new Resource();
    Mockito.when(fromResource.apply(r)).thenReturn(m);
    Assertions.assertEquals(m, converter.convertFromResource(r));
    Mockito.verify(fromResource, Mockito.times(1)).apply(ArgumentMatchers.eq(r));
  }

  @Test
  void functionFromModelInvokedForEachElementInList() {
    Model model1 = new Model();
    Model model2 = new Model();
    List<Model> models = Stream.of(model1, model2).collect(Collectors.toList());
    converter.listFromModels(models);
    ArgumentCaptor<Model> argument = ArgumentCaptor.forClass(Model.class);

    Mockito.verify(fromModel, Mockito.times(2)).apply(argument.capture());
    List<Model> invoked = argument.getAllValues();
    Assertions.assertTrue(invoked.contains(model1));
    Assertions.assertTrue(invoked.contains(model2));
  }

  @Test
  void functionFromResourceInvokedForEachElementInList() {
    Resource res1 = new Resource();
    Resource res2 = new Resource();
    List<Resource> resources = Stream.of(res1, res2).collect(Collectors.toList());
    converter.listFromResources(resources);
    ArgumentCaptor<Resource> argument = ArgumentCaptor.forClass(Resource.class);

    Mockito.verify(fromResource, Mockito.times(2)).apply(argument.capture());
    List<Resource> invoked = argument.getAllValues();
    Assertions.assertTrue(invoked.contains(res1));
    Assertions.assertTrue(invoked.contains(res2));
  }
}
