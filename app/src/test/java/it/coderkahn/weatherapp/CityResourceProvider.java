package it.coderkahn.weatherapp;

import it.coderkahn.weatherapp.ports.apis.CityResource;

public class CityResourceProvider {
  public static final CityResource MILAN =
      new CityResource(3173435, "Milano", "IT", 9.18951, 45.464272);
  public static final CityResource ROME =
      new CityResource(3169070, "Roma", "IT", 12.4839, 41.894741);
}
