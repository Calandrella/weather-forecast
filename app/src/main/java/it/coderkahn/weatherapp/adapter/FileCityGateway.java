package it.coderkahn.weatherapp.adapter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.coderkahn.patterns.monad.Validator;
import it.coderkahn.weatherapp.core.City;
import it.coderkahn.weatherapp.ports.CityGateway;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class FileCityGateway implements CityGateway {

  private final String filename;

  public FileCityGateway(String filename) {
    this.filename = filename;
    Validator.of(getClass().getClassLoader().getResource(filename))
        .validate(Objects::nonNull, String.format("File with name %s not found", filename))
        .get();
  }

  @Override
  public List<City> getAvailableCities() {
    try (InputStream stream =
        Validator.of(getClass().getClassLoader().getResourceAsStream(filename))
            .validate(Objects::nonNull, String.format("File with name %s not found", filename))
            .get()) {
      List<City> cities = new ObjectMapper().readValue(stream, new TypeReference<List<City>>() {});
      cities.sort(Comparator.comparingInt(City::getId));
      return cities;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
