package it.coderkahn.weatherapp.adapter.weatherService;

import it.coderkahn.patterns.monad.Validator;
import java.util.Objects;

public abstract class AbstractForecastQuery implements ForecastQuery {
  private String baseUrl;

  public AbstractForecastQuery(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  @Override
  public String getBaseUrl() {
    return baseUrl;
  }

  @Override
  public final String endpointUri(String apiKey) {
    return getBaseUrl()
        + FORECAST_PATH
        + QUESTION_MARK
        + Validator.of(getQuery()).validate(Objects::nonNull, "Query cannot be null").get()
        + AND
        + getUnitString()
        + appKey()
        + apiKey;
  }

  private String getUnitString() {
    return getUnitFormat() != null ? "units=" + getUnitFormat().getStringValue() + AND : "";
  }

  protected abstract String getQuery();
}
