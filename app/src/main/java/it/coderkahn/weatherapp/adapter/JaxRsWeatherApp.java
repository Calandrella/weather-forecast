package it.coderkahn.weatherapp.adapter;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import it.coderkahn.weatherapp.adapter.weatherService.OpenApiWeather;
import it.coderkahn.weatherapp.core.CachedCityWeather;
import it.coderkahn.weatherapp.core.CityServiceImpl;
import it.coderkahn.weatherapp.core.CityWeatherImpl;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class JaxRsWeatherApp extends Application {
  private static String BASE_URL = "http://api.openweathermap.org/data/2.5";
  private final Set<Object> singletons = new HashSet<>();

  public JaxRsWeatherApp() {
    CityWeatherImpl cityWeatherService = new CityWeatherImpl(new OpenApiWeather(BASE_URL));
    FileCityGateway cityGateway = new FileCityGateway("city.list.json");
    singletons.addAll(
        Collections.unmodifiableList(
            Stream.of(
                    new RestCityWeatherAdapter(
                        new CityServiceImpl(new CachedCityGw(cityGateway)),
                        new CachedCityWeather(cityWeatherService)),
                    new JacksonJaxbJsonProvider(),
                    new DefaultExceptionHandler(),
                    new ResourceNotFoundHandler())
                .collect(Collectors.toList())));
  }

  public Set<Object> getSingletons() {
    return Collections.unmodifiableSet(singletons);
  }
}
