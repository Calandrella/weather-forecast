package it.coderkahn.weatherapp.adapter;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import it.coderkahn.weatherapp.core.City;
import it.coderkahn.weatherapp.ports.CityGateway;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CachedCityGw implements CityGateway {
  private final CityGateway cityGateway;
  private Cache<String, List<City>> cache;

  public CachedCityGw(CityGateway cityGateway, int maximumSize, int duration, TimeUnit timeUnit) {
    this.cityGateway = cityGateway;
    cache =
        Caffeine.newBuilder().maximumSize(maximumSize).expireAfterWrite(duration, timeUnit).build();
  }

  public CachedCityGw(CityGateway cityGateway) {
    this(cityGateway, 1_000, 1, TimeUnit.HOURS);
  }

  @Override
  public List<City> getAvailableCities() {
    return cache.get("ALL", s -> cityGateway.getAvailableCities());
  }
}
