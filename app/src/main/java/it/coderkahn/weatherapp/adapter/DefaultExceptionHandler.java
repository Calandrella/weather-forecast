package it.coderkahn.weatherapp.adapter;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class DefaultExceptionHandler implements ExceptionMapper<RuntimeException> {
  @Override
  public Response toResponse(RuntimeException exception) {
    exception.printStackTrace();
    return Response.status(500).entity(new ErrorMessage(exception.getMessage())).build();
  }

  private static class ErrorMessage {
    private String message;

    private ErrorMessage(String message) {
      this.message = message;
    }

    public String getMessage() {
      return message;
    }
  }
}
