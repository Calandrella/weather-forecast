package it.coderkahn.weatherapp.adapter.weatherService;

public interface Query {
  String QUESTION_MARK = "?";
  String AND = "&";

  Unit getUnitFormat();

  String getBaseUrl();

  String endpointUri(String apiKey);

  default String appKey() {
    return "appid=";
  }

  enum Unit {
    STANDARD("standard"),
    IMPERIAL("imperial"),
    METRIC("metric");
    private final String stringValue;

    Unit(String s) {
      this.stringValue = s;
    }

    public static Unit fromString(String s) {
      for (Unit unit : values()) {
        if (unit.stringValue.equals(s)) {
          return unit;
        }
      }
      throw new IllegalArgumentException("No UnitFormat was found with value:" + s);
    }

    public String getStringValue() {
      return stringValue;
    }
  }
}
