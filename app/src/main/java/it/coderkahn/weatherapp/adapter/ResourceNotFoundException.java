package it.coderkahn.weatherapp.adapter;

public class ResourceNotFoundException extends RuntimeException {
  private final String message;

  public ResourceNotFoundException(String message) {
    this.message = message;
  }

  @Override
  public String getMessage() {
    return String.format("Resource not found. %s", message);
  }
}
