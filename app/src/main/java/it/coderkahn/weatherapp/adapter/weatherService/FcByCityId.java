package it.coderkahn.weatherapp.adapter.weatherService;

public class FcByCityId extends AbstractForecastQuery {
  private final int cityId;

  public FcByCityId(String baseUrl, int cityId) {
    super(baseUrl);
    this.cityId = cityId;
  }

  @Override
  protected String getQuery() {
    return "id=" + cityId;
  }

  /**
   * Defaults to METRIC == Celsius
   *
   * @return {@see Unit#METRIC}
   */
  @Override
  public Unit getUnitFormat() {
    return Unit.METRIC;
  }
}
