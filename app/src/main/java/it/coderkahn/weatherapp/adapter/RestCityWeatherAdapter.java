package it.coderkahn.weatherapp.adapter;

import it.coderkahn.weatherapp.ports.apis.CityResource;
import it.coderkahn.weatherapp.ports.apis.CityService;
import it.coderkahn.weatherapp.ports.apis.CityWeather;
import it.coderkahn.weatherapp.ports.apis.WeatherMetrics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("data")
public class RestCityWeatherAdapter {

  public static final int FORECAST_DAYS_NR = 3;

  private final CityService cityService;
  private final CityWeather weatherService;

  public RestCityWeatherAdapter(CityService cityService, CityWeather weatherService) {
    this.cityService = cityService;
    this.weatherService = weatherService;
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getCities(
      @Context UriInfo uriInfo, @QueryParam("offset") int offset, @QueryParam("limit") int limit) {
    List<CityResource> cities = cityService.getCities();
    Stream<CityResource> cityStream =
        cities.stream().skip(offset).peek(c -> initCityLink(c, uriInfo));
    GenericEntity<List<CityResource>> entity =
        limit == 0
            ? new GenericEntity<>(cityStream.collect(Collectors.toList()), List.class)
            : new GenericEntity<>(cityStream.limit(limit).collect(Collectors.toList()), List.class);
    List<Link> links = new ArrayList<>();
    links.add(
        Link.fromUriBuilder(uriInfo.getRequestUriBuilder()).title("Self").rel("self").build());
    if (limit != 0 && offset + limit <= cities.size())
      links.add(
          Link.fromUriBuilder(
                  uriInfo
                      .getAbsolutePathBuilder()
                      .queryParam("offset", String.valueOf(offset + limit))
                      .queryParam("limit", String.valueOf(limit)))
              .rel("next")
              .title("Next")
              .build());
    return Response.ok().entity(entity).links(links.toArray(new Link[] {})).build();
  }

  @GET
  @Path("{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response dataById(@Context UriInfo uriInfo, @PathParam("id") int id) {
    List<CityResource> cities = cityService.getCities();
    CityResource cityResource =
        cities
            .stream()
            .filter(c -> c.getId() == id)
            .findFirst()
            .orElseThrow(() -> new ResourceNotFoundException("Invalid cityResource id"));
    WeatherMetrics entity = weatherService.forecastWeatherMetrics(cityResource, FORECAST_DAYS_NR);
    Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();
    return Response.ok().entity(entity).links(self).build();
  }

  /*
    @GET
    @Path("{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response dataByName(@Context UriInfo uriInfo, @PathParam("name") String name) {
      String cityName, country;
      if (name.contains(",")) {
        String[] split = name.split(",");
        if (split.length != 2) return Response.status(Response.Status.BAD_REQUEST).build();
        cityName = split[0];
        country = split[1];
      } else {
        cityName = name;
        country = "";
      }
      CityResource cityResource =
          cityService
              .getCities()
              .stream()
              .filter(c -> c.getName().equals(cityName) && c.getCountry().equals(country))
              .findFirst()
              .orElseThrow(() -> new ResourceNotFoundException("Invalid cityResource id"));
      WeatherMetrics entity = weatherService.forecastWeatherMetrics(cityResource, 3);
      Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();
      return Response.ok().entity(entity).links(self).build();
    }
  */
  private void initCityLink(CityResource cityResource, UriInfo uriInfo) {
    UriBuilder uriBuilder = uriInfo.getRequestUriBuilder();
    uriBuilder.path(Integer.toString(cityResource.getId()));
    uriInfo.getQueryParameters().forEach((s, strings) -> uriBuilder.replaceQueryParam(s, null));
    Link.Builder linkBuilder = Link.fromUriBuilder(uriBuilder);
    Link selfLink = linkBuilder.rel("self").param("Method", "GET").build();
    cityResource.setLinks(Collections.singletonList(selfLink));
  }
}
