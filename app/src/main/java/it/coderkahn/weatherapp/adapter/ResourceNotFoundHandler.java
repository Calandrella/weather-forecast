package it.coderkahn.weatherapp.adapter;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

class ResourceNotFoundHandler implements ExceptionMapper<ResourceNotFoundException> {

  @Override
  public Response toResponse(ResourceNotFoundException exception) {
    exception.printStackTrace();
    return Response.status(Response.Status.NOT_FOUND).build();
  }
}
