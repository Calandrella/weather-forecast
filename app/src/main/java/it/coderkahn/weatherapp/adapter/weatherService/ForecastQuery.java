package it.coderkahn.weatherapp.adapter.weatherService;

public interface ForecastQuery extends Query {
  String FORECAST_PATH = "/forecast";
}
