package it.coderkahn.weatherapp.adapter.weatherService;

import static java.time.temporal.ChronoUnit.DAYS;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import it.coderkahn.patterns.monad.PredicateUtil;
import it.coderkahn.weatherapp.core.City;
import it.coderkahn.weatherapp.ports.WeatherApi;
import it.coderkahn.weatherapp.ports.apis.WeatherMetrics;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ValueRange;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Stream;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.client.ClientConfig;

public class OpenApiWeather implements WeatherApi {
  private String KEY;

  private final String baseUrl;

  public OpenApiWeather(String baseUrl) {
    this.baseUrl = baseUrl;
    this.KEY = System.getenv("appKey");
  }

  @Override
  public WeatherMetrics getWeatherMetrics(City city, int forecastDay) {
    FcByCityId fc = new FcByCityId(baseUrl, city.getId());
    final JacksonJsonProvider jacksonJsonProvider =
        new JacksonJaxbJsonProvider()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    final Client client = ClientBuilder.newClient(new ClientConfig(jacksonJsonProvider));
    WebTarget target = client.target(fc.endpointUri(KEY));
    Invocation.Builder request = target.request(MediaType.APPLICATION_JSON);
    request.acceptEncoding("UTF-8");
    Forecast forecast = request.get(Forecast.class);
    ValueRange daily = ValueRange.of(6, 18);
    Supplier<Stream<Weather>> forecastStreamSupplier =
        () ->
            forecast
                .getList()
                .stream()
                .filter(
                    weather ->
                        weather.getDt().isBefore(ZonedDateTime.now().plus(forecastDay, DAYS)));
    double avgDailyTemp =
        forecastStreamSupplier
            .get()
            .filter(weather -> daily.isValidValue(weather.getDt().getHour()))
            .mapToDouble(Weather::getTemp)
            .average()
            .orElse(Double.NaN);
    double avgNightlyTemp =
        forecastStreamSupplier
            .get()
            .filter(PredicateUtil.negate(weather -> daily.isValidValue(weather.getDt().getHour())))
            .mapToDouble(Weather::getTemp)
            .average()
            .orElse(Double.NaN);
    double averagePressure =
        forecastStreamSupplier.get().mapToDouble(Weather::getPressure).average().orElse(Double.NaN);
    return new WeatherMetrics(
        city.getName(), city.getCountry(), avgDailyTemp, avgNightlyTemp, averagePressure);
  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Forecast {
    private List<Weather> list;

    public List<Weather> getList() {
      return list;
    }
  }

  public static class Weather {
    private ZonedDateTime dt;
    private double temp, pressure, temp_min, temp_max;

    public double getTemp() {
      return temp;
    }

    public double getPressure() {
      return pressure;
    }

    public double getTemp_min() {
      return temp_min;
    }

    public double getTemp_max() {
      return temp_max;
    }

    @SuppressWarnings("unchecked")
    @JsonProperty("main")
    private void unpackNested(Map<String, Object> main) {
      this.temp = toDouble(main.get("temp"));
      this.temp_min = toDouble(main.get("temp_min"));
      this.temp_max = toDouble(main.get("temp_max"));
      this.pressure = toDouble(main.get("pressure"));
    }

    private double toDouble(Object number) {
      if (number instanceof Double) {
        return (Double) number;
      } else if (number instanceof Integer) {
        return (Integer) number;
      } else {
        throw new RuntimeException("Unexpected type: " + number.getClass());
      }
    }

    @JsonProperty("dt")
    public void setDt(int dt) {
      Instant i = Instant.ofEpochSecond(dt);
      this.dt = ZonedDateTime.ofInstant(i, ZoneOffset.UTC);
    }

    public ZonedDateTime getDt() {
      return dt;
    }
  }
}
