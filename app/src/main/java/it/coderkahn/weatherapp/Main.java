package it.coderkahn.weatherapp;

import it.coderkahn.weatherapp.adapter.JaxRsWeatherApp;

public class Main {
  public static void main(String[] args) throws Exception {
    try (InMemoryServer server = InMemoryServer.create(JaxRsWeatherApp.class)) {
      server.start(Integer.parseInt(System.getenv("appPort")));
      server.join();
    }
  }
}
