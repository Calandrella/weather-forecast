package it.coderkahn.weatherapp;

import it.coderkahn.patterns.monad.Validator;
import java.util.Objects;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.servlet.ServletProperties;

public class InMemoryServer implements AutoCloseable {

  private static final int DEFAULT_PORT = 8080;

  private final Class<? extends Application> application;

  private Server server;

  private String path;

  public static InMemoryServer create(Class<? extends Application> app) {
    return new InMemoryServer(
        Validator.of(app).validate(Objects::nonNull, "Application class cannot be null").get());
  }

  private InMemoryServer(Class<? extends Application> app) {
    ApplicationPath appPath = app.getAnnotation(ApplicationPath.class);
    path = appPath == null ? "/" : appPath.value();
    this.application = app;
  }

  public void start(int port) {
    int port1 = port == 0 ? DEFAULT_PORT : port;
    server = new Server(new QueuedThreadPool(300));
    ServerConnector connector = new ServerConnector(server);
    connector.setPort(port1);
    server.setConnectors(new Connector[] {connector});

    ServletContextHandler ctx = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
    ctx.setContextPath(path);
    server.setHandler(ctx);

    ServletHolder serHol = ctx.addServlet(ServletContainer.class, "/*");
    serHol.setInitOrder(1);
    serHol.setInitParameter(
        ServletProperties.JAXRS_APPLICATION_CLASS, application.getCanonicalName());

    try {
      server.setStopAtShutdown(true);
      server.setStopTimeout(5_000);
      server.start();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public void join() {
    try {
      server.join();
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  public String getAppPath() {
    return path.startsWith("/") ? path : "/" + path;
  }

  @Override
  public void close() throws Exception {
    if (server != null) {
      server.stop();
      server = null;
    }
  }
}
