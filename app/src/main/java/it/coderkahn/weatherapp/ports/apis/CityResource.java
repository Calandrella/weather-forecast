package it.coderkahn.weatherapp.ports.apis;

import it.coderkahn.patterns.monad.Validator;
import java.util.List;
import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class CityResource {
  private final int id;
  private final CityName cityName;
  private final Coord coord;

  private List<Link> links;

  public CityResource(int id, String name, String country, double longitude, double latitude) {
    this.id = Validator.of(id).validate(i -> i > 0, ("Invalid id for city")).get();
    this.cityName =
        Validator.of(new CityName(name, country))
            .validate(
                cityName -> cityName.name != null,
                (String.format("City with id %d. City name cannot be null", id)))
            .get();
    this.coord = new Coord(longitude, latitude);
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return cityName.name;
  }

  public String getCountry() {
    return cityName.country;
  }

  public double getLatitude() {
    return coord.lat;
  }

  public double getLongitude() {
    return coord.lon;
  }

  public void setLinks(List<Link> links) {
    this.links = links;
  }

  @XmlJavaTypeAdapter(Link.JaxbAdapter.class)
  public List<Link> getLinks() {
    return links;
  }

  private static class CityName {
    private final String name, country;

    private CityName(String name, String country) {
      this.name = name;
      this.country = country;
    }
  }

  private static class Coord {
    private final double lon, lat;

    private Coord(double lon, double lat) {
      this.lon = lon;
      this.lat = lat;
    }
  }
}
