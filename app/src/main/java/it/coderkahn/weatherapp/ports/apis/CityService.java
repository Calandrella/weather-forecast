package it.coderkahn.weatherapp.ports.apis;

import java.util.List;

public interface CityService {
  List<CityResource> getCities();
}
