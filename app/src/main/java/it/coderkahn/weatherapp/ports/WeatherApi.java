package it.coderkahn.weatherapp.ports;

import it.coderkahn.weatherapp.core.City;
import it.coderkahn.weatherapp.ports.apis.WeatherMetrics;

public interface WeatherApi {
  WeatherMetrics getWeatherMetrics(City city, int forecastDay);
}
