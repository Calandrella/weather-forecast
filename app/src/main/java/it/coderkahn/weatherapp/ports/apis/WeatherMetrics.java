package it.coderkahn.weatherapp.ports.apis;

import java.util.List;
import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class WeatherMetrics {
  private final double dailyAverage, nightlyAverage, pressure;

  private final String cityName;

  private final String country;

  private List<Link> links;

  public WeatherMetrics(
      String name, String country, double dailyAverage, double nightlyAverage, double pressure) {
    this.cityName = name;
    this.country = country;
    this.dailyAverage = dailyAverage;
    this.nightlyAverage = nightlyAverage;
    this.pressure = pressure;
  }

  public void setLinks(List<Link> links) {
    this.links = links;
  }

  @XmlJavaTypeAdapter(Link.JaxbAdapter.class)
  public List<Link> getLinks() {
    return links;
  }

  public double getDailyAverage() {
    return dailyAverage;
  }

  public double getNightlyAverage() {
    return nightlyAverage;
  }

  public double getPressure() {
    return pressure;
  }

  public String getCityName() {
    return cityName;
  }

  public String getCountry() {
    return country;
  }
}
