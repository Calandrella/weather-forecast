package it.coderkahn.weatherapp.ports.apis;

public interface CityWeather {
  WeatherMetrics forecastWeatherMetrics(CityResource cityResource, int forecastDaysNr);
}
