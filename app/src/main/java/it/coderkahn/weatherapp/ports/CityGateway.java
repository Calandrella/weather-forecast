package it.coderkahn.weatherapp.ports;

import it.coderkahn.weatherapp.core.City;
import java.util.List;

public interface CityGateway {
  List<City> getAvailableCities();
}
