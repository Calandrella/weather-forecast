package it.coderkahn.weatherapp.util;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Converter<T, D> {
  private final Function<T, D> fromModel;

  private final Function<D, T> fromResource;

  public Converter(final Function<T, D> fromModel, final Function<D, T> fromResource) {
    this.fromModel = fromModel;
    this.fromResource = fromResource;
  }

  public final D convertFromModels(final T dto) {
    return fromModel.apply(dto);
  }

  public final T convertFromResource(final D model) {
    return fromResource.apply(model);
  }

  public final List<D> listFromModels(Collection<T> dtos) {
    return dtos.stream().map(this::convertFromModels).collect(Collectors.toList());
  }

  public final List<T> listFromResources(Collection<D> models) {
    return models.stream().map(this::convertFromResource).collect(Collectors.toList());
  }
}
