package it.coderkahn.weatherapp.core;

public class City {
  private int id;

  private String name;

  private String country;

  private Coord coord;

  public City() {}

  public City(int id, String name, String country, Coord coord) {
    this.id = id;
    this.name = name;
    this.country = country;
    this.coord = coord;
  }

  public Coord getCoord() {
    return coord;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getCountry() {
    return country;
  }

  public double getLongitude() {
    return coord.getLon();
  }

  public double getLatitude() {
    return coord.getLat();
  }
}
