package it.coderkahn.weatherapp.core;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import it.coderkahn.weatherapp.ports.apis.CityResource;
import it.coderkahn.weatherapp.ports.apis.CityWeather;
import it.coderkahn.weatherapp.ports.apis.WeatherMetrics;
import java.util.concurrent.TimeUnit;

public class CachedCityWeather implements CityWeather {
  private final CityWeather cityWeatherService;
  private Cache<Integer, WeatherMetrics> cache;

  public CachedCityWeather(CityWeather cityWeather) {
    this(cityWeather, 1_000, 2, TimeUnit.DAYS);
  }

  public CachedCityWeather(
      CityWeather cityWeatherService, int maximumSize, int duration, TimeUnit timeUnit) {
    this.cityWeatherService = cityWeatherService;
    cache =
        Caffeine.newBuilder().maximumSize(maximumSize).expireAfterWrite(duration, timeUnit).build();
  }

  @Override
  public WeatherMetrics forecastWeatherMetrics(CityResource cityResource, int forecastDaysNr) {
    return cache.get(
        cityResource.getId(),
        s -> cityWeatherService.forecastWeatherMetrics(cityResource, forecastDaysNr));
  }
}
