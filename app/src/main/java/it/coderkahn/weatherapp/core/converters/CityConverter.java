package it.coderkahn.weatherapp.core.converters;

import it.coderkahn.weatherapp.core.City;
import it.coderkahn.weatherapp.core.Coord;
import it.coderkahn.weatherapp.ports.apis.CityResource;
import it.coderkahn.weatherapp.util.Converter;

public class CityConverter extends Converter<City, CityResource> {
  public CityConverter() {
    super(
        city ->
            new CityResource(
                city.getId(),
                city.getName(),
                city.getCountry(),
                city.getLongitude(),
                city.getLatitude()),
        cityResource ->
            new City(
                cityResource.getId(),
                cityResource.getName(),
                cityResource.getCountry(),
                new Coord(cityResource.getLongitude(), cityResource.getLatitude())));
  }
}
