package it.coderkahn.weatherapp.core;

import it.coderkahn.patterns.monad.Validator;
import it.coderkahn.weatherapp.core.converters.CityConverter;
import it.coderkahn.weatherapp.ports.WeatherApi;
import it.coderkahn.weatherapp.ports.apis.CityResource;
import it.coderkahn.weatherapp.ports.apis.CityWeather;
import it.coderkahn.weatherapp.ports.apis.WeatherMetrics;
import java.util.Objects;

public class CityWeatherImpl implements CityWeather {
  private WeatherApi weatherApi;

  public CityWeatherImpl(WeatherApi weatherApi) {
    this.weatherApi = weatherApi;
  }

  @Override
  public WeatherMetrics forecastWeatherMetrics(CityResource cityResource, int forecastDaysNr) {
    Validator.of(cityResource).validate(Objects::nonNull, "CityResource cannot be null").get();
    Validator.of(forecastDaysNr)
        .validate(days -> 0 < days && days <= 3, "You can only ask from 1 to 3 days forecast")
        .get();
    return weatherApi.getWeatherMetrics(
        new CityConverter().convertFromResource(cityResource), forecastDaysNr);
  }
}
