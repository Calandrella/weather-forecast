package it.coderkahn.weatherapp.core;

import it.coderkahn.patterns.monad.Validator;
import it.coderkahn.weatherapp.core.converters.CityConverter;
import it.coderkahn.weatherapp.ports.CityGateway;
import it.coderkahn.weatherapp.ports.apis.CityResource;
import it.coderkahn.weatherapp.ports.apis.CityService;
import java.util.List;

public class CityServiceImpl implements CityService {
  private final CityGateway cityGateway;

  public CityServiceImpl(CityGateway cityGateway) {
    this.cityGateway = cityGateway;
  }

  @Override
  public List<CityResource> getCities() {
    CityConverter cityConverter = new CityConverter();
    return cityConverter.listFromModels(
        Validator.of(cityGateway.getAvailableCities())
            .validate(cities -> cities != null && !cities.isEmpty(), "Not found")
            .get());
  }
}
