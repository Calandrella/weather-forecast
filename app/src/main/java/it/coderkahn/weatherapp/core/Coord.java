package it.coderkahn.weatherapp.core;

public class Coord {
  private double lon;

  private double lat;

  public Coord() {}

  public Coord(double longitude, double latitude) {
    this.lon = longitude;
    this.lat = latitude;
  }

  public double getLat() {
    return lat;
  }

  public double getLon() {
    return lon;
  }
}
